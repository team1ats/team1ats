package com.ats.team1ats.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UtamaController {

	@RequestMapping("/")
	public String utama() {
		String html = "utama"; 
		return html;
	}
}
