package com.ats.team1ats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Team1atsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Team1atsApplication.class, args);
	}

}
